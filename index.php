<?php

ipLog();

echo file_get_contents('index.html');

function ipLog()
{
    $data = $_SERVER['REQUEST_METHOD'] === 'POST' ?
        file_get_contents('php://input') :
        $_SERVER['REMOTE_ADDR'];

    file_put_contents(
        __DIR__ . '/../logs/ips.log',
        sprintf("[%s] %s\n", date('Y-m-d H:i:s'), $data),
        FILE_APPEND
    );
}

