FROM php:8-fpm

RUN mkdir -p /var/www/logs
RUN chown -R www-data:www-data /var/www/logs
VOLUME ["/var/www/logs"]